# CarCar

Team:

* Aaron Guiang - Automobile Sales
* Danielle Aharonov - Automobile Service

## Design

## Service microservice

I followed the design template given to us in the assignment and I made small tweaks. I started with building my view functions then worked on my frontend so that I can display the right things.

## Sales microservice

I started off doing the backend work like creating the models, views, etc. Once I had a solid foundation on the backend side of things I moved to working with the frontend. The main priority to to make sure they all flowed together without any errors.
