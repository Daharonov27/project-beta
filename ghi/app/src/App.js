import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechniciansList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './CreateAppointment';
import AppointmentList from './AppointmentList';
import ServiceHistoryList from './ServiceHistoryList';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './CreateManufacturer';
import ModelList from './ModelList';
import ModelForm from './CreateModel';
import AutomobileList from './AutomobileList';
import AutomobileForm from './CreateAutomobile';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesRecordForm from './SalesRecordForm';
import SalesRecordList from './SalesRecordList';
import SalespersonHistory from './SalespersonHistory';

function App() {
  return (
	<BrowserRouter>
	  <Nav />
	  <div className="container">
		<Routes>
		  <Route path="/" element={<MainPage />} />
		  <Route path="technicians" element={<TechnicianList />} />
		  <Route path="technicians/new" element={<TechnicianForm />} />
		  <Route path="appointments/new" element={<AppointmentForm />} />
		  <Route path="appointments" element={<AppointmentList />} />
		  <Route path="appointments/history" element={<ServiceHistoryList />} />
		  <Route path="manufacturers" element={<ManufacturerList />} />
		  <Route path="manufacturers/new" element={<ManufacturerForm />} />
		  <Route path="models" element={<ModelList />} />
		  <Route path="models/new" element={<ModelForm />} />
		  <Route path="automobiles" element={<AutomobileList />} />
		  <Route path="automobiles/new" element={<AutomobileForm />} />
		  <Route path="salespersons/new" element={<SalespersonForm />} />
		  <Route path="salespersons" element={<SalespersonList />} />
		  <Route path="customers/new" element={<CustomerForm />} />
		  <Route path="customers" element={<CustomerList />} />
		  <Route path="sales/new" element={<SalesRecordForm />} />
		  <Route path="sales" element={<SalesRecordList />} />
		  <Route path="sales/history" element={<SalespersonHistory />} />
		</Routes>
	  </div>
	</BrowserRouter>
  );
}


export default App;
