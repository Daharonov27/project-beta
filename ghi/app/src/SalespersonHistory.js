import React, { useState, useEffect } from 'react';


function SalespersonHistory() {
  const [salesHistory, setSalesHistory] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [salespeople, setSalespeople] = useState([]);

    const fetchSalespeople = async () => {
      const response = await fetch('http://localhost:8090/api/salespersons/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespersons);
      } else {
        console.error(response);
    }

    };
    useEffect(() => {
    fetchSalespeople();
    loadSalesHistory();
  }, []);

  const loadSalesHistory = async () => {
    const response = await fetch(`http://localhost:8090/api/sales/`);
    if (response.ok) {
      const data = await response.json();
      setSalesHistory(data.salesrecords);
    } else {
      console.error(response);
    }
  };

  const handleChange = (event) => {
    setSelectedSalesperson(event.target.value);
  };

  return (
    <div>
      <h1 className="mt-4 mb-4">Salesperson History</h1>
      <select onChange={handleChange} id="salesperson-select" name="Salesperson" className="form-select mb-3">
        <option value="">Choose Salesperson</option>
        {salespeople.map((salesperson) => {
          return (
            <option key={salesperson.id} value={salesperson.id}>
              {salesperson.name}
            </option>
          );
        })}
      </select>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Automobile VIN</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {salesHistory.filter(sale => (selectedSalesperson ? sale.salesperson.id==selectedSalesperson : sale)).map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.name}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalespersonHistory;
