import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO


def get_autos():
    #sending GET request to endpoint
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    #break up content as JSON
    content = json.loads(response.content)
    for auto in content['autos']:
        #Update AutomobileVO in database with location data
        AutomobileVO.objects.update_or_create(
            import_href=auto["href"],
            defaults={
                    "color": auto["color"],
                    "year": auto["year"],
                    "vin": auto["vin"],
            }
        )
        print(AutomobileVO)



def poll():
    while True:
        print('Sales poller polling for data')
        try:
            # Write your polling logic, here
            get_autos()
        except Exception as e:
            print("There's an error", e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
