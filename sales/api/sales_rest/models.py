from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href=models.CharField(max_length=200, blank=True, null=True, unique=True)
    color=models.CharField(max_length=50)
    year=models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class SalesRecord(models.Model):
    price = models.PositiveIntegerField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name ="sales_records",
        on_delete=models.CASCADE,
    )

    salesperson=models.ForeignKey(
        Salesperson,
        related_name="sales_records",
        on_delete=models.CASCADE,
    )

    customer=models.ForeignKey(
        Customer,
        related_name="sales_records",
        on_delete=models.CASCADE,

    )
